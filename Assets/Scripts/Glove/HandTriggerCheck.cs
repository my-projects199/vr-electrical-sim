using System;
using System.Collections;
using System.Collections.Generic;
using BNG;
using UnityEngine;
using Object = System.Object;

public class HandTriggerCheck : MonoBehaviour
{
    private Material defaultHandMaterial;
    private Material defaultGloveMaterial;
    private HandStates hStates;
    //private bool isEquipped = false;
    //private bool isHolding = false;
    private bool isGloveDamaged;
    private GameObject parent;
    private string handPath;

    [SerializeField] private GameObject spawnedObject;
    [SerializeField] private Material highlightMaterial;
    [SerializeField] private Material warningMaterial;

    //public ControllerHand handSide;

    void Start()
    {
        hStates = gameObject.GetComponent<HandStates>();
        parent = transform.parent.gameObject;

        if (hStates.handSide == ControllerHand.Left)
        {
            handPath = "CustomHandLeftBlackNew/hands_r_gloves_mat06/";
        }
        else if(hStates.handSide == ControllerHand.Right)
        {
            handPath = "CustomHandRightBlackNew/hands_r_gloves_mat06/";
        }
        
        defaultHandMaterial = parent.transform.Find(handPath + "handsModel/Rhand")
            .gameObject.GetComponent<SkinnedMeshRenderer>().material;
        defaultGloveMaterial = parent.transform.Find(handPath + "Golves_low")
            .gameObject.GetComponent<SkinnedMeshRenderer>().material;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Glove")
        {
            //Debug.Log("Enter!");
            GameObject glove = other.gameObject;
            if(glove.GetComponent<GloveStates>().isHeld 
               && !hStates.isEquipped
               && glove.GetComponent<GloveStates>().handSide == hStates.handSide)
            {
                glove.GetComponent<GloveWear>().isInCorrectHandTrigger = true;
                parent.transform.Find(handPath + "Golves_low").gameObject.GetComponent<SkinnedMeshRenderer>().material =
                    highlightMaterial;
                parent.transform.Find(handPath + "handsModel/Rhand").gameObject.GetComponent<SkinnedMeshRenderer>().material =
                    highlightMaterial;
            }
            
            if(glove.GetComponent<GloveStates>().isHeld 
               && !hStates.isEquipped
               && glove.GetComponent<GloveStates>().handSide != hStates.handSide)
            {
                parent.transform.Find(handPath + "Golves_low").gameObject.GetComponent<SkinnedMeshRenderer>().material =
                    warningMaterial;
                parent.transform.Find(handPath + "handsModel/Rhand").gameObject.GetComponent<SkinnedMeshRenderer>().material =
                    warningMaterial;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Glove")
        {
            //Debug.Log("Stay!");
            GameObject glove = other.gameObject;
            if(glove.GetComponent<GloveStates>().isWeared
                && !hStates.isHolding
               && glove.GetComponent<GloveStates>().handSide == hStates.handSide)
            {
                hStates.isEquipped = true;
                isGloveDamaged = glove.GetComponent<GloveStates>().isDamaged;
                parent.transform.Find(handPath + "Golves_low").gameObject.SetActive(false);
                parent.transform.Find(handPath + "Golves_low_weared").gameObject.SetActive(true);
                parent.transform.Find(handPath + "handsModel/Rhand").gameObject.SetActive(false);
                parent.transform.Find(handPath + "handsModel/Rhand_weared").gameObject.SetActive(true);
                
                Destroy(other.gameObject);
            }
        }

        if (other.gameObject.name == "Grabber")
        {
            if (other.gameObject.GetComponent<Grabber>().HandSide == ControllerHand.Left)
            {
                if (hStates.isEquipped
                    && !hStates.isHolding
                    && other.gameObject.GetComponent<Grabber>().HandSide != hStates.handSide
                    && (InputBridge.Instance.LeftGripDown)
                )
                {
                    Debug.Log("Taking glove");
                    var glove = GameObject.Instantiate(spawnedObject, other.gameObject.transform.position, Quaternion.identity);
                    glove.GetComponent<GloveStates>().handSide = hStates.handSide;
                    hStates.isEquipped = false;
                    parent.transform.Find(handPath + "Golves_low").gameObject.SetActive(true);
                    parent.transform.Find(handPath + "Golves_low_weared").gameObject.SetActive(false);
                    parent.transform.Find(handPath + "handsModel/Rhand").gameObject.SetActive(true);
                    parent.transform.Find(handPath + "handsModel/Rhand_weared").gameObject.SetActive(false);
                }
            }
            else if (other.gameObject.GetComponent<Grabber>().HandSide == ControllerHand.Right)
            {
                if (hStates.isEquipped
                    && !hStates.isHolding
                    && other.gameObject.GetComponent<Grabber>().HandSide != hStates.handSide
                    && (InputBridge.Instance.RightGripDown)
                )
                {
                    Debug.Log("Taking glove");
                    var glove = GameObject.Instantiate(spawnedObject, other.gameObject.transform.position, Quaternion.identity);
                    glove.GetComponent<GloveStates>().handSide = hStates.handSide;
                    glove.GetComponent<GloveStates>().isDamaged = isGloveDamaged;
                    hStates.isEquipped = false;
                    parent.transform.Find(handPath + "Golves_low").gameObject.SetActive(true);
                    parent.transform.Find(handPath + "Golves_low_weared").gameObject.SetActive(false);
                    parent.transform.Find(handPath + "handsModel/Rhand").gameObject.SetActive(true);
                    parent.transform.Find(handPath + "handsModel/Rhand_weared").gameObject.SetActive(false);
                }
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Glove")
        {
            //Debug.Log("Exit!");
            GameObject glove = other.gameObject;
            if(!hStates.isEquipped)
            {
                glove.GetComponent<GloveWear>().isInCorrectHandTrigger = false;
                parent.transform.Find(handPath + "Golves_low").gameObject.GetComponent<SkinnedMeshRenderer>().material =
                    defaultGloveMaterial;
                parent.transform.Find(handPath + "handsModel/Rhand").gameObject.GetComponent<SkinnedMeshRenderer>()
                        .material =
                    defaultHandMaterial;
            }
        }
    }
}
