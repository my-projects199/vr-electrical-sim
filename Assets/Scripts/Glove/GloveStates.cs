using System;
using System.Collections;
using System.Collections.Generic;
using BNG;
using UnityEngine;

public class GloveStates : MonoBehaviour
{
    //сторона перчатки
    public ControllerHand handSide;
    
    //повреждена/не повреждена
    public bool isDamaged;
    
    //держат ли перчатку
    [NonSerialized] public bool isHeld = false;
    
    //одерта ли перчатка
    [NonSerialized] public bool isWeared = false;

    //какой рукой держат перчатку
    [NonSerialized] public ControllerHand holdingHand;
    
    public void OnGrabEvent()
    {
        holdingHand = gameObject.GetComponent<Grabbable>().GetClosestGrabber().HandSide;
        isHeld = true;
    }
    
    public void OnReleaseEvent()
    {
        holdingHand = ControllerHand.None;
        isHeld = false;
    }
}
