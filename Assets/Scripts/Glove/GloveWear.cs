using System;
using System.Collections;
using System.Collections.Generic;
using BNG;
using UnityEngine;

public class GloveWear : MonoBehaviour
{
    [NonSerialized] public bool isInCorrectHandTrigger = false;

    private GloveStates gStates;
    //private Grabbable _grabbable;

    void Start()
    {
        gStates = gameObject.GetComponent<GloveStates>();
        //_grabbable = gameObject.GetComponent<Grabbable>();
    }

    public void OnReleaseEvent()
    {
        if (isInCorrectHandTrigger)
        {
            gStates.isWeared = true;
        }
    }
}
