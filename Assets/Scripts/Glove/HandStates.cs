using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class HandStates : MonoBehaviour
{
    public bool isEquipped;

    public bool isHolding;

    public ControllerHand handSide;

    private void Start()
    {
        isEquipped = false;
        isHolding = false;
    }


    public void OnGrabEvent()
    {
        isHolding = true;
    }

    public void OnReleaseEvent()
    {
        isHolding = false;
    }
}
