using System;
using System.Collections;
using System.Collections.Generic;
using BNG;
using UnityEngine;

public class GloveCheck : MonoBehaviour
{
    //private ControllerHand myControllerHand;
    private Animation animationComponent;
    private MeshCollider meshColliderComponent;
    private AudioSource audioComponent;
    private Grabbable grabbableComponent;
    private GloveStates gStates;
    private bool isBlended;

    [NonSerialized] public bool isInHeadTrigger;
    
    void Start()
    {
        gStates = gameObject.GetComponent<GloveStates>();
        isInHeadTrigger = false;
        isBlended = false;
        animationComponent = gameObject.GetComponent<Animation>();
        audioComponent = gameObject.GetComponent<AudioSource>();
        grabbableComponent = gameObject.GetComponent<Grabbable>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isInHeadTrigger)
        {
            if ((InputBridge.Instance.LeftTriggerDown && gStates.holdingHand == ControllerHand.Left) 
                || (InputBridge.Instance.RightTriggerDown && gStates.holdingHand == ControllerHand.Right))
            {
                animationComponent.Play("GloveBlow");
                if(!isBlended)
                    isBlended = true;
            }
        }

        if (gStates.isDamaged && isBlended)
        {
            if(!audioComponent.isPlaying)
                audioComponent.Play(1);
        }

        if (isBlended && gStates.holdingHand == ControllerHand.None)
        {
            animationComponent.Play("GloveUsual");
            isBlended = false;
            audioComponent.Stop();
        }

        if (((InputBridge.Instance.LeftTriggerUp && gStates.holdingHand == ControllerHand.Left) 
             || (InputBridge.Instance.RightTriggerUp && gStates.holdingHand == ControllerHand.Right)) 
            && isBlended)
        {
            animationComponent.Play("GloveUsual");
            isBlended = false;
            audioComponent.Stop();
        }
    }

}
