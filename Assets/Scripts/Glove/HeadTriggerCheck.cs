using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadTriggerCheck : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Glove")
        {
            other.gameObject.GetComponent<GloveCheck>().isInHeadTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Glove")
        {
            other.gameObject.GetComponent<GloveCheck>().isInHeadTrigger = false;
        }
    }
}
