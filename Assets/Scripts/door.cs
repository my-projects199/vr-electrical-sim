using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class door : MonoBehaviour
{
    DoorHelper doorHelperGO;
    TestScript testScriptGO;

    [SerializeField] GameObject leftHandTrigger;
    [SerializeField] GameObject rightHandTrigger;

    bool glassessSnapEvent = false;
    bool helmetSnapEvent = false;

    bool isLocked;

    public void GlassesSnapEvent(bool isSnapped)
    {
        glassessSnapEvent = isSnapped;
    }

    public void HelmetSnapEvent(bool isSnapped)
    {
        helmetSnapEvent = isSnapped;
    }

    void Start()
    {
        doorHelperGO = gameObject.GetComponent<DoorHelper>();
        testScriptGO = GameObject.FindObjectOfType<TestScript>();

        doorHelperGO.DegreesTurnToOpen = 100f;
        isLocked = true;
    }
    
    void Update()
    {
        if(testScriptGO.counter == 5 
            && isLocked 
            && glassessSnapEvent 
            && helmetSnapEvent
            && leftHandTrigger.GetComponent<HandStates>().isEquipped
            && rightHandTrigger.GetComponent<HandStates>().isEquipped
            )
        {
            OpenDoor(true);
        }
        else if((!glassessSnapEvent 
            || !helmetSnapEvent 
            || !leftHandTrigger.GetComponent<HandStates>().isEquipped
            || !rightHandTrigger.GetComponent<HandStates>().isEquipped)
            && !isLocked)
        {
            OpenDoor(false);
        }
    }

    private void OpenDoor(bool toOpen)
    {
        if(toOpen)
        {
            doorHelperGO.DegreesTurnToOpen = 15f;
            isLocked = false;
        }
        else
        {
            doorHelperGO.DegreesTurnToOpen = 100f;
            isLocked = true;
        }
    }
}
