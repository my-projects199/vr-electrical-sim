//To work, be necessarily to connect Snap Event and specify filters!!

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dampingGlassesFilter : MonoBehaviour
{
    private bool snap_event = false;
    private Color _default_color;
    private Color _color;
    private float dampingTimer = timer;
    [SerializeField] private static float timer = 3.0f;

    public GameObject glassess;
    

    public void start_snap_event()
    {
        snap_event = true;
    }

    public void stop_snap_event()
    {
        snap_event = false;
    }

    public void damping()
    {
        _color = glassess.GetComponent<Renderer>().material.color;

        dampingTimer -= Time.deltaTime;

        if (_color.a > 0 & dampingTimer <= 0)
        {
            _color.a -= 0.001f;
            glassess.GetComponent<Renderer>().material.color = _color;
        }

        if (_color.a < 0)
        {
            glassess.SetActive(false);
        }
    }

    public void undamping()
    {
        _color = _default_color;
        glassess.GetComponent<Renderer>().material.color = _color;

        dampingTimer = timer;

        glassess.SetActive(true);
    }

    public void Default_color()
    {
        _default_color = glassess.GetComponent<Renderer>().material.color;
    }

    void Start()
    {
        Default_color();
    }

    void Update()
    {
        if (snap_event == true)
        {
            damping();
        }
        else
        {
            undamping();
        }
    }
}
