using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BNG;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using UnityEngine.Networking;
using System.Text;

public struct TaskStruct
{
    public int id;
    public string question;
    public string answer;
    public string[] options;
}

public class TestScript : MonoBehaviour
{
    public int counter;

    public const int VARIANTS_MAX = 6;
    [SerializeField] private float TIME_PERIOD = 30f;
    [SerializeField] private float TIME_TO_WAIT = 3f;
    [SerializeField] private GameObject ButtonsGO;
    private GameObject questionGO;
    private GameObject timerGO;
    private GameObject variantsListGO;

    private float timeLeft;
    private bool enableTimer;

    TaskStruct currTask;
    
    // Start is called before the first frame update
    void Start()
    {
        
        //CreateQuestionList();
        timeLeft = TIME_PERIOD;
        enableTimer = true;
        counter = 0;

        questionGO = gameObject.transform.Find("Question").gameObject;
        timerGO = gameObject.transform.Find("Timer").gameObject;
        variantsListGO = gameObject.transform.Find("Variants list").gameObject;

        StartCoroutine(ResetQuestion());
    }

    // Update is called once per frame
    void Update()
    {
        if (enableTimer)
        {
            timeLeft -= Time.deltaTime;
            timerGO.GetComponent<Text>().text = ((int)timeLeft).ToString();
            if (timeLeft < 0)
            {
                if (!variantsListGO.GetComponent<VariantsListScript>().isVariantChosen())
                {
                    counter = 0;
                    gameObject.transform.Find("Counter").gameObject.GetComponent<Text>().text = "Верных ответов: " + counter;
                }
                StartCoroutine(ResetQuestion());
            }
        }
    }

    void EnableButtons(bool val)
    {
        foreach(Transform b in ButtonsGO.transform.GetComponentsInChildren<Transform>())
        {
            var inner = b.gameObject.transform.Find("InnerButton");
            if(inner)
            {
                inner.GetComponent<BNG.Button>().enabled = val;
            }
        }
    }

    

    IEnumerator waiter()
    {
        enableTimer = false;
        variantsListGO.GetComponent<VariantsListScript>().ShowResults();
        UpdateCounter();

        EnableButtons(false);
        yield return new WaitForSeconds(TIME_TO_WAIT);
        EnableButtons(true);

        StartCoroutine( ResetQuestion());
        enableTimer = true;
    }

    public void UpdateCounter()
    {
        if (variantsListGO.GetComponent<VariantsListScript>().checkRightAnswer())
        {
            counter++;
            gameObject.transform.Find("Counter").gameObject.GetComponent<Text>().text = "Верных ответов: " + counter;

            if (counter == 5)
            {
                enableTimer = false;
                gameObject.SetActive(false);
                gameObject.GetComponentInParent<Transform>().Find("End text").gameObject.SetActive(true);
            }
        }
        else
        {
            counter = 0;
            gameObject.transform.Find("Counter").gameObject.GetComponent<Text>().text = "Верных ответов: " + counter;
        }
    }

    IEnumerator ResetQuestion()
    {
        timeLeft = TIME_PERIOD;
        //questionGO.GetComponent<Text>().text = qList[n][0];
        yield return RequestGetQuestion();
        variantsListGO.GetComponent<VariantsListScript>().ResetVariants(currTask);
        questionGO.GetComponent<Text>().text = currTask.question;
    }
    
    public void UpdateTest()
    {
        StartCoroutine(waiter());
    }

    IEnumerator RequestGetQuestion()
    {
        string url = "http://109.123.155.175:9001/get-random-topic-task";;
        string body = "{\"sectionId\":2, \"certAreaId\":20}";

        UnityWebRequest request = UnityWebRequest.Post(url, body);
        byte[] postBytes = Encoding.UTF8.GetBytes(body);
        UploadHandler uploadHandler = new UploadHandlerRaw(postBytes);
        request.uploadHandler = uploadHandler;
        //request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        currTask = JsonUtility.FromJson<TaskStruct>(request.downloadHandler.text);
        request.Dispose();
    }
}
