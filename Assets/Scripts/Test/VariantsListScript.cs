using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VariantsListScript : MonoBehaviour
{
    private int vars_number;

    private List<GameObject> varsList = new List<GameObject>();
    private string rightAnswer;

    private void Start()
    {
        vars_number = TestScript.VARIANTS_MAX;
        for (int i = 1; i <= vars_number; i++)
        {
            GameObject tmp = gameObject.transform.Find("Var" + i).gameObject;
            if(tmp)
            {
                varsList.Add(tmp);
            }
        }
    }

    void ResetBools()
    {
        foreach (var i in varsList)
        {
            i.GetComponent<VariantScript>().isClicked = false;
            i.GetComponent<Image>().color = Color.white;
        }
    }
    public bool isVariantChosen()
    {
        foreach (var i in varsList)
        {
            if (i.GetComponent<VariantScript>().isClicked)
                return true;
        }
        return false;
    }

    public void ResetVariants(TaskStruct task)
    {
        ResetBools();
        for (int i = 0; i < vars_number; i++)
        {
            var vText = varsList[i].transform.Find("Text").gameObject;
            if(i < task.options.Length)
                vText.GetComponent<Text>().text = task.options[i];
            else
                vText.GetComponent<Text>().text = "";
        }
        rightAnswer = task.answer;
    }

    public bool checkRightAnswer()
    {
        bool isCorrect = false;
        int clickCounter = 0;
        
        for (int i = 0; i < vars_number; i++)
        {
            
            if (varsList[i].GetComponent<VariantScript>().isClicked)
            {
                clickCounter++;
                var userInput = varsList[i].transform.Find("Text").gameObject.GetComponent<Text>().text;
                if (userInput != rightAnswer)
                {
                    isCorrect = false;
                    break;
                }

                if (userInput == rightAnswer)
                {
                    isCorrect = true;
                }
            }
        }

        if (clickCounter > 1)
            isCorrect = false;

        return isCorrect;
    }

    public void ShowResults()
    {
        for (int i = 0; i < vars_number; i++)
        {
            var userInput = varsList[i].transform.Find("Text").gameObject.GetComponent<Text>().text;
            if (userInput == rightAnswer)
            {
                varsList[i].GetComponent<Image>().color = Color.green;
            }

            if (varsList[i].GetComponent<VariantScript>().isClicked)
            {
                if (userInput != rightAnswer)
                {
                    varsList[i].GetComponent<Image>().color = Color.red;
                }
            }
        }
    }

    public void SetAnswersFrozen(bool val)
    {
        foreach (var i in varsList)
        {
            i.GetComponent<VariantScript>().isFrozen = val;
        }
    }

}
