using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariantScript : MonoBehaviour
{
    public bool isClicked { get; set; } = false;
    public bool isFrozen { get; set; } = false;
}
